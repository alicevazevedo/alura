package br.com.caelum.livraria.webservice.test;

import java.rmi.RemoteException;

import br.com.caelum.livraria.webservice.LivrariaWS;
import br.com.caelum.livraria.webservice.LivrariaWSProxy;
import br.com.caelum.livraria.webservice.Livro;

public class TestRequestSoap {

	public static void main(String[] args) throws RemoteException {
		LivrariaWS cliente = new LivrariaWSProxy();
		
		Livro[] livrosPeloNome = cliente.getLivrosPeloNome("Arquitetura");
		
		for (Livro livro : livrosPeloNome) {
			System.out.println(livro.getTitulo() + " - " + livro.getAutor().getNome());
		}
	}
}
