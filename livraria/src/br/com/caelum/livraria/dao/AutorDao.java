package br.com.caelum.livraria.dao;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.caelum.livraria.interceptador.LogInterceptador;
import br.com.caelum.livraria.modelo.Autor;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER) // padrao, nao precisa configurar
//@Interceptors({LogInterceptador.class})
public class AutorDao {

	@PersistenceContext
	private EntityManager manager;
//	@Inject
//	private Banco banco;
	
	@PostConstruct
	void aposCriacao() {
		System.out.println("AutorDao criado");
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void salva(Autor autor) {
		System.out.println("Salvando autor " + autor.getNome());
		
//		banco.save(autor);
		manager.persist(autor);
		System.out.println("Salvou autor " + autor.getNome());

	}
	
	public List<Autor> todosAutores() {
//		return banco.listaAutores();
		return manager.createQuery("select a from Autor a", Autor.class).getResultList();
	}

	public Autor buscaPelaId(Integer autorId) {
//		Autor autor = this.banco.buscaPelaId(autorId);
		Autor autor = manager.find(Autor.class, autorId);
		return autor;
	}
	
}
