package br.com.caelum.livraria.bean;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.caelum.livraria.dao.AutorDao;
import br.com.caelum.livraria.dao.LivrariaException;
import br.com.caelum.livraria.modelo.Autor;

@Stateless
public class AutorService {

	@Inject
	AutorDao dao;
	
	// required
	public void adiciona(Autor autor) {
		// outras regras
		dao.salva(autor);
		
		// simula exceção lançada por serviço externo
//		throw new RuntimeException("erro simulado");
		
//		throw new LivrariaException();
	}

	// required
	public List<Autor> todosAutores() {
		return dao.todosAutores();
	}
}
