package oo.aula6;

import oo.aula4.Divida;
import oo.aula5.Documento;

public interface ArmazenadorDeDivida {
	void salva(Divida divida);
	Divida carrega(Documento documento);
}
