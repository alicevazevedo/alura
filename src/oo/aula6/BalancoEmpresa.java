package oo.aula6;

import oo.aula3.Pagamento;
import oo.aula4.Divida;
import oo.aula5.Documento;

public class BalancoEmpresa {
	private ArmazenadorDeDivida dividas;
	
	public BalancoEmpresa(ArmazenadorDeDivida dividas) {
		this.dividas = dividas;
	}

	public void registraDivida(Divida divida) {
		dividas.salva(divida);
	}

	public void pagaDivida(Documento documentoCredor, Pagamento pagamento) {
		Divida divida = dividas.carrega(documentoCredor);
		if (divida != null) {
			divida.registra(pagamento); // O objeto Divida registra o pagamento
		}
		dividas.salva(divida);
	}
}
