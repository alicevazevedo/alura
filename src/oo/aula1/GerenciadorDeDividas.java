package oo.aula1;

public class GerenciadorDeDividas {

	public void efetuaPagamento(Divida divida, double valor, String pagador, String cnpjPagador) {
		Pagamento pagamento = new Pagamento();
		pagamento.setPagador(pagador);
		pagamento.setCnpjPagador(cnpjPagador);
		pagamento.setValor(valor);
		divida.registra(pagamento);
	}
}
