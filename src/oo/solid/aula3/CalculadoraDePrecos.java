package oo.solid.aula3;

public class CalculadoraDePrecos {
	
	private ServicoDeEntrega entrega;
	private TabelaDePreco tabelaDePreco;

	public CalculadoraDePrecos(TabelaDePreco tabelaDePreco, ServicoDeEntrega entrega) {
		this.tabelaDePreco = tabelaDePreco;
		this.entrega = entrega;
	}

	public double calcula(Compra produto) {

        double desconto = tabelaDePreco.descontoPara(produto.getValor());
        double frete = entrega.para(produto.getCidade());

        return produto.getValor() * (1-desconto) + frete;
    }
}
