package oo.solid.aula3;

public class Compra {
	
	private String cidade;
	private double valor;

	public Compra(double valor, String cidade) {
		this.valor = valor;
		this.cidade = cidade;
	}

	public double getValor() {
		return valor;
	}

	public String getCidade() {
		return cidade;
	}

}
