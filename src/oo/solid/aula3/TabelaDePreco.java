package oo.solid.aula3;

public interface TabelaDePreco {
	
	public double descontoPara(double valor);
}
