package oo.solid.aula3;

public interface ServicoDeEntrega {

	public double para(String cidade);
}
