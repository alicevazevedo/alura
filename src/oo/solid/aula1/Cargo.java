package oo.solid.aula1;

public enum Cargo {
	DESENVOLVEDOR(new DezOuVintePorCento()),
    DBA(new QuinzeOuVinteECincoPorCento()),
    TESTER(new QuinzeOuVinteECincoPorCento());
    
    private RegraDeCalculo regraDeCalculo;

	Cargo(RegraDeCalculo regraDeCalculo) {
		this.regraDeCalculo = regraDeCalculo;
		
	}
	
	public RegraDeCalculo getRegraDeCalculo() {
		return regraDeCalculo;
	}
}
