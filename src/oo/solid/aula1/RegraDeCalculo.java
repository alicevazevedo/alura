package oo.solid.aula1;

public interface RegraDeCalculo {
	public double calcula(Funcionario funcionario);
}
