package oo.solid.aula5;

public class ContaComum {
	
	private ManipuladorDeSaldo manipulador;
	
    public ContaComum() {
        manipulador = new ManipuladorDeSaldo();
    }

    public void deposita(double valor) {
    	manipulador.deposita(valor);
    }

    public void saca(double valor) {
        manipulador.saca(valor);
    }

    public void somaInvestimento(){
        manipulador.somaInvestimento();
    }

    public double getSaldo() {
        return manipulador.getSaldo();
    }

}
