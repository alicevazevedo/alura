package oo.solid.aula5;

public class ContaDeEstudante {
	
	ManipuladorDeSaldo manipulador;
	
	private int milhas;
	
	public ContaDeEstudante() {
		manipulador = new ManipuladorDeSaldo();
	}

    public void deposita(double valor) {
        manipulador.deposita(valor);
        this.milhas += (int)valor;
    }

    public int getMilhas() {
        return milhas;
    }
    
    public void somaInvestimento(){
        manipulador.somaInvestimento();
    }
    
    public double getSaldo() {
        return manipulador.getSaldo();
    }

}
