package oo.solid.aula2;

public interface AcaoAposGerarNota {
	public void executa(NotaFiscal notaFiscal);
}
