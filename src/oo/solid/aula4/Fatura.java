package oo.solid.aula4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Fatura {
	
	private double valorPago;
	private String cliente;
    private double valor;
    private List<Pagamento> pagamentos;
    private boolean pago;

    public Fatura(String cliente, double valor) {
        this.cliente = cliente;
        this.valor = valor;
        this.pagamentos = new ArrayList<Pagamento>();
        this.pago = false;
    }

    public String getCliente() {
        return cliente;
    }

    public double getValor() {
        return valor;
    }
    
    public void adicionaPagamento(Pagamento pagamento) {
    	valorPago += pagamento.getValor();
    	this.pagamentos.add(pagamento);
    	if (valorPago >= valor) {
    		pago = true;
    	}
    }

    public List<Pagamento> getPagamentos() {
        return Collections.unmodifiableList(pagamentos);
    }

    public boolean isPago() {
        return pago;
    }

    public void setPago(boolean pago) {
        this.pago = pago;
    }

}
