package oo.aula5;

public class Cpf implements Documento {

	private String valor;

	public Cpf(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}
	
	@Override
	public String toString() {
		return valor;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Cpf) {
			Cpf outro = (Cpf) obj;
			return this.valor.equals(outro.getValor());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return valor.hashCode();
	}
	
	public boolean ehValido() {
		//TODO
		double rand = Math.random();
		if (rand < 0.5) return true;
		return false;
	}
}
