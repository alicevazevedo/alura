package oo.aula5;

import oo.aula3.Pagamento;
import oo.aula3.Pagamentos;

public class TestaPagamentos {
	public static void main(String[] args) {
		Pagamentos pagamentos = new Pagamentos();
		Pagamento p1 = new Pagamento();
		Pagamento p2 = new Pagamento();
		p1.setValor(105);
		p2.setValor(25);
		
		pagamentos.registra(p1);
		pagamentos.registra(p2);
		
		System.out.println("Valor total pago: " + pagamentos.getValorPago());
		
		Iterable<Pagamento> lista = pagamentos;
		for (Pagamento p : lista) {
			System.out.println(p.getValor());
		}
	}
	
}
