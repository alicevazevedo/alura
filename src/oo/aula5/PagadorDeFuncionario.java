package oo.aula5;

public class PagadorDeFuncionario {

	public void pagaAssalariado(Assalariado assalariado) {
		assalariado.paga(assalariado.getSalarioBase() + assalariado.getExtras());
	}
}
