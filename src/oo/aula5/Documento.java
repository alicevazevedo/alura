package oo.aula5;

public interface Documento {
	boolean ehValido();
	String getValor();
}
