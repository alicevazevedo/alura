package oo.aula5;

public interface Assalariado {
	public void paga(double valor);
    public double getSalarioBase();
    public double getExtras();
}
