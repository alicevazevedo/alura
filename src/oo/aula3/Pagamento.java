package oo.aula3;

import java.util.Calendar;

public class Pagamento extends Movimentacao {
	private double valor;
	private Calendar data;
	
	private String favorecido;
    private String formaDePagamento;
	
	public double getValor() {
		return valor;
	}
	
	public Calendar getData() {
		return data;
	}
	
	public void setValor(double valor) {
		this.valor = valor;
	}
}
