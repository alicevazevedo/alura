package oo.aula4;

import java.util.HashMap;
import java.util.Map;

import oo.aula2.Cnpj;
import oo.aula3.Pagamento;
import oo.aula5.Documento;

public class BalancoEmpresa {
	private Map<Documento, Divida> dividas = new HashMap<Documento, Divida>();

	public void registraDivida(Divida divida) {
		dividas.put(divida.getDocumentoCredor(), divida);
	}

	public void pagaDivida(Cnpj cnpjCredor, Pagamento pagamento) {
		Divida divida = dividas.get(cnpjCredor);
		if (divida != null) {
			divida.registra(pagamento); // O objeto Divida registra o pagamento
		}
	}
}
