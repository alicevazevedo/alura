package oo.aula4;

import java.text.NumberFormat;
import java.util.Locale;

import oo.aula3.Pagamento;
import oo.aula5.Cnpj;

public class TesteRelatorio {
	public static void main(String[] args) {
		Divida divida = new Divida();
		divida.setCredor("Cliente");
		divida.setDocumentoCredor(new Cnpj("000000000"));
		divida.setTotal(3000);

		Pagamento p = new Pagamento();
		p.setValor(100);
		divida.registra(p);

		// temos uma formata��o que usa os padr�es brasileiros
		NumberFormat formatadorBrasileiro = NumberFormat
				.getCurrencyInstance(new Locale("pt", "BR"));

		// temos agora uma formata��o que usa os padr�es dos Estados Unidos
		NumberFormat formatadorAmericano = NumberFormat
				.getCurrencyInstance(new Locale("en", "US"));

		RelatorioDeDivida relatorio = new RelatorioDeDivida(divida);

		// geramos o relat�rio com o formatador brasileiro
		relatorio.geraRelatorio(formatadorBrasileiro);

		// e agora com o formatador americano na mesma inst�ncia de
		// RelatorioDeDivida
		relatorio.geraRelatorio(formatadorAmericano);
	}
}
