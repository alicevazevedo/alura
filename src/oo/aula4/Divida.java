package oo.aula4;

import oo.aula3.Pagamento;
import oo.aula3.Pagamentos;
import oo.aula5.Documento;

public class Divida {
	private double total;
    private String credor;
    private Documento documentoCredor;
    private Pagamentos pagamentos = new Pagamentos();

    public double getTotal() {
		return total;
	}
    public void setTotal(double total) {
		this.total = total;
	}
    
    public Documento getDocumentoCredor() {
		return documentoCredor;
	}
    
    public void setDocumentoCredor(Documento documentoCredor) {
		this.documentoCredor = documentoCredor;
	}
    
    public String getCredor() {
		return credor;
	}
    
    public void setCredor(String credor) {
		this.credor = credor;
	}

    public double getValorAPagar() {
        return this.total - this.pagamentos.getValorPago();
    }

    // novo método responsável por registrar um pagamento
    public void registra(Pagamento pagamento) {
        pagamentos.registra(pagamento);
    }
}
