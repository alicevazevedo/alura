package oo.aula2;

public class Cnpj {
	private String valor;
	
	public Cnpj(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}
	
	public boolean cnpjValido() {
        return primeiroDigitoVerificadorDoCnpj() == primeiroDigitoCorretoParaCnpj()
                && segundoDigitoVerificadorDoCnpj() == segundoDigitoCorretoParaCnpj();
    }
	
	private int primeiroDigitoCorretoParaCnpj() {
        // Calcula o primeiro dígito verificador correto para
        // o CNPJ armazenado no atributo valor
        return 0;
    }
    private int primeiroDigitoVerificadorDoCnpj() {
        // Extrai o primeiro dígito verificador do CNPJ armazenado
        // no atributo valor
        return 0;
    }
    private int segundoDigitoCorretoParaCnpj() {
        // Calcula o segundo dígito verificador correto para
        // o CNPJ armazenado no atributo valor
        return 0;
    }
    private int segundoDigitoVerificadorDoCnpj() {
        // Extrai o segundo dígito verificador do CNPJ armazenado
        // no atributo valor
        return 0;
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (obj instanceof Cnpj) {
    		Cnpj outro = (Cnpj) obj;
    		return valor.equals(outro.getValor());
    	}
    	return false;
    }
    
    @Override
    public int hashCode() {
    	return valor.hashCode();
    }
}
