package design.patterns.aula5.state;

public class Positiva implements EstadoDeUmaConta {

	@Override
	public void deposita(Conta conta, double valor) {
		conta.saldo += valor * 0.98;
	}

	@Override
	public double saque(Conta conta, double valor) {
		conta.saldo -= valor;
		if (conta.saldo < valor)
			conta.estado = new Negativa();
		
		return valor;
	}
}
