package design.patterns.aula5.state;

public class Conta {

	protected double saldo;
	protected EstadoDeUmaConta estado;
	
	public Conta(double saldo) {
		this.saldo = saldo;
		if (saldo < 0) estado = new Negativa();
		else estado = new Positiva();
	}
	
	public void deposita(double valor) {
		estado.deposita(this, valor);
	}
	
	public double saca(double valor) {
		return estado.saque(this, valor);
	}
}
