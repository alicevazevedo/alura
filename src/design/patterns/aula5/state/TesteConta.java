package design.patterns.aula5.state;

public class TesteConta {
	public static void main(String[] args) {
		Conta conta = new Conta(100);
		
		conta.saca(50);
		System.out.println("saldo: " + conta.saldo);
		System.out.println(conta.estado);
		
		conta.saca(70);
		System.out.println("saldo: " + conta.saldo);
		System.out.println(conta.estado);
		
		conta.deposita(10);
		System.out.println("saldo: " + conta.saldo);
		System.out.println(conta.estado);
		
		conta.deposita(100);
		System.out.println("saldo: " + conta.saldo);
		System.out.println(conta.estado);

		conta.saca(200);
		System.out.println("saldo: " + conta.saldo);
		System.out.println(conta.estado);
		
		conta.saca(20);
		System.out.println("saldo: " + conta.saldo);
		System.out.println(conta.estado);
	}
}
