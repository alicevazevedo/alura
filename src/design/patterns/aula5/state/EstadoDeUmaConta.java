package design.patterns.aula5.state;

public interface EstadoDeUmaConta {
	
	public void deposita(Conta conta, double valor);
	public double saque(Conta conta, double valor);
}
