package design.patterns.aula5.state;

public class Negativa implements EstadoDeUmaConta {

	@Override
	public void deposita(Conta conta, double valor) {
		conta.saldo += valor * 0.95;
		if (conta.saldo > 0) conta.estado = new Positiva();
	}

	@Override
	public double saque(Conta conta, double valor) {
		throw new RuntimeException("Conta negativa nao permite saque");
	}
}
