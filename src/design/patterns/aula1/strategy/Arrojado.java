package design.patterns.aula1.strategy;
public class Arrojado implements Investimento {
	@Override
	public double calcular(ContaCorrente contaCorrente) {
		java.util.Random r = new java.util.Random();
		double probabilidade = r.nextDouble();
		
		// tem 20% de chances de retornar 5%
		if (probabilidade < 0.2) {
			return contaCorrente.getSaldo() * 0.05;
		}
		
		// tem 30% de chances de retornar 3%
		if (probabilidade >= 0.2 && probabilidade < 0.5) {
			return contaCorrente.getSaldo() * 0.03;
		}
		
		// e 50% de chances de retornar 0.6%
		return contaCorrente.getSaldo() * 0.006;
	}
}
