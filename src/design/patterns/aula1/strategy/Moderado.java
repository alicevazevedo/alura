package design.patterns.aula1.strategy;
public class Moderado implements Investimento {
	@Override
	public double calcular(ContaCorrente contaCorrente) {
		java.util.Random r = new java.util.Random();
		// tem 50% de chances de retornar 2.5%
		if (r.nextDouble() < 0.5) {
			return contaCorrente.getSaldo() * 0.025;
		}
		// e 50% de chances de retornar 0.7%
		return contaCorrente.getSaldo() * 0.007;
	}
}
