package design.patterns.aula1.strategy;
public class ICCC extends Imposto {

	public ICCC() {
		
	}
	
	public ICCC(Imposto outroImposto) {
		super(outroImposto);
	}
	
	@Override
	public double calcular(Orcamento orcamento) {
		double valor = orcamento.getValor();
		if (valor < 1000) {
			return valor * 0.05;
		}
		if (valor <= 3000) {
			return valor * 0.07;
		}
		return valor * 0.08 + 30;
	}

}
