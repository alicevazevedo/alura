package design.patterns.aula1.strategy;

public abstract class Imposto {
	protected Imposto outroImposto;
	
	public Imposto() {
	}
	
	public Imposto(Imposto outroImposto) {
		this.outroImposto = outroImposto;
	}
	

	protected double calculoDoOutroImposto(Orcamento orcamento) {
		if (outroImposto == null) return 0;
		return outroImposto.calcular(orcamento);
	}
	
	public abstract double calcular(Orcamento orcamento);
}
