package design.patterns.aula1.strategy;

public interface Investimento {
	public double calcular(ContaCorrente contaCorrente);
}
