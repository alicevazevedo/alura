package design.patterns.aula1.strategy;

public class RealizadorDeInvestimentos {
	public void realizaInvestimento(ContaCorrente contaCorrente, Investimento investimento) {
		double resultado = investimento.calcular(contaCorrente);
		double aposImpostos = resultado * 0.75;
		contaCorrente.deposita(aposImpostos);
		System.out.println("Saldo atualizado: " + contaCorrente.getSaldo());
	}
}
