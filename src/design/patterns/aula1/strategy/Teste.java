package design.patterns.aula1.strategy;
import design.patterns.aula1.strategy.CalculadorDeImposto;
import design.patterns.aula1.strategy.ICCC;
import design.patterns.aula1.strategy.Imposto;
import design.patterns.aula1.strategy.Orcamento;


public class Teste {
	public static void main(String[] args) {
		Imposto iccc = new ICCC();
		CalculadorDeImposto calculadorDeImposto = new CalculadorDeImposto();

		Orcamento orcamento = new Orcamento(500);
		calculadorDeImposto.realizaCalculo(orcamento, iccc);

		orcamento = new Orcamento(1000);
		calculadorDeImposto.realizaCalculo(orcamento, iccc);

		orcamento = new Orcamento(2000);
		calculadorDeImposto.realizaCalculo(orcamento, iccc);

		orcamento = new Orcamento(3000);
		calculadorDeImposto.realizaCalculo(orcamento, iccc);

		orcamento = new Orcamento(4000);
		calculadorDeImposto.realizaCalculo(orcamento, iccc);
	}
}
