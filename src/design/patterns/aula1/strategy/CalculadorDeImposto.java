package design.patterns.aula1.strategy;

public class CalculadorDeImposto {
	
	public void realizaCalculo(Orcamento orcamento, Imposto imposto) {
		double resultado = imposto.calcular(orcamento);
		System.out.println(resultado);
	}
}
