package design.patterns.aula1.strategy;

public class ICMS extends Imposto {

	public ICMS() {
		
	}
	
	public ICMS(Imposto outroImposto) {
		super(outroImposto);
	}
	
	@Override
	public double calcular(Orcamento orcamento) {
		return orcamento.getValor() * 0.05 + 50 + calculoDoOutroImposto(orcamento);
	}
}
