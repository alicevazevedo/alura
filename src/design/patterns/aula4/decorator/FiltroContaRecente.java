package design.patterns.aula4.decorator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import design.patterns.aula2.chain.Conta;


public class FiltroContaRecente extends Filtro {
	
	public FiltroContaRecente() {
		super();
	}
	
	public FiltroContaRecente(Filtro outroFiltro) {
		super(outroFiltro);
	}

	@Override
	public List<Conta> filtra(List<Conta> contas) {
		List<Conta> contasFraudulentas = new ArrayList<Conta>();
		for (Conta c : contas) {
			Calendar data = Calendar.getInstance();
			data.setTime(c.getDataAbertura());
			if (data.get(Calendar.MONTH) == Calendar.getInstance().get(
					Calendar.MONTH)
					&& data.get(Calendar.YEAR) == Calendar.getInstance().get(
							Calendar.YEAR)) {
				contasFraudulentas.add(c);
			}
		}
		contasFraudulentas.addAll(outrasFraudes(contas));
		return contasFraudulentas;
	}

}
