package design.patterns.aula4.decorator;

import design.patterns.aula1.strategy.ICMS;
import design.patterns.aula1.strategy.Orcamento;

public class TesteDeImpostosComplexos {
	
	public static void main(String[] args) {
//		Imposto iss = new ISS(new ICMS());
		ImpostoMuitoAlto imposto = new ImpostoMuitoAlto(new ICMS());
		
		Orcamento orcamento = new Orcamento(1000);
		double valor = imposto.calcular(orcamento);
		
		System.out.println(valor);
	}
}
