package design.patterns.aula4.decorator;

import java.util.ArrayList;
import java.util.List;

import design.patterns.aula2.chain.Conta;


public class FiltroSaldoMaiorQue500MilReais extends Filtro {

	public FiltroSaldoMaiorQue500MilReais() {
		super();
	}
	
	public FiltroSaldoMaiorQue500MilReais(Filtro outroFiltro) {
		super(outroFiltro);
	}
	
	@Override
	public List<Conta> filtra(List<Conta> contas) {
		List<Conta> contasFraudulentas = new ArrayList<Conta>();
		for (Conta conta : contas) {
			if (conta.getSaldo() > 500000) {
				contasFraudulentas.add(conta);
			}
		}
		contasFraudulentas.addAll(outrasFraudes(contas));
		return contasFraudulentas;
	}

}
