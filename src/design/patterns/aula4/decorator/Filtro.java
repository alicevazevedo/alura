package design.patterns.aula4.decorator;

import java.util.ArrayList;
import java.util.List;

import design.patterns.aula2.chain.Conta;


public abstract class Filtro {
	protected Filtro outroFiltro;
	
	public Filtro() {
	}
	
	public Filtro(Filtro outroFiltro) {
		this.outroFiltro = outroFiltro;
	}
	
	protected List<Conta> outrasFraudes(List<Conta> contas) {
		if (outroFiltro != null)
			return outroFiltro.filtra(contas);
		return new ArrayList<Conta>();
	}
	
	public abstract List<Conta> filtra(List<Conta> contas);
}
