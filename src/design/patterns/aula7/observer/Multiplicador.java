package design.patterns.aula7.observer;

import design.patterns.aula6.builder.NotaFiscal;

public class Multiplicador implements AcaoAposGerarNota {

	private double fator;

	public Multiplicador(double fator) {
		this.fator = fator;
	}
	
	@Override
	public void executa(NotaFiscal nf) {
		System.out.println("apos multiplicar: " + nf.getValorBruto() * this.fator);
	}

}
