package design.patterns.aula7.observer;

import design.patterns.aula6.builder.NotaFiscal;

public class EnviadorDeEmail implements AcaoAposGerarNota {

	@Override
	public void executa(NotaFiscal nf) {
		System.out.println("email");
	}
}
