package design.patterns.aula7.observer;

import design.patterns.aula6.builder.NotaFiscal;

public class EnviadorDeSms implements AcaoAposGerarNota {

	public void executa(NotaFiscal nf) {
		System.out.println("sms");		
	}
}
