package design.patterns.aula7.observer;

import java.util.Arrays;

import design.patterns.aula6.builder.ItemDaNota;
import design.patterns.aula6.builder.NotaFiscal;
import design.patterns.aula6.builder.NotaFiscalBuilder;


public class TesteAcoes {
	public static void main(String[] args) {
		AcaoAposGerarNota[] acoes = new AcaoAposGerarNota[]{new EnviadorDeEmail(), new EnviadorDeSms(), new NotaFiscalDao(), new Impressora(), new Multiplicador(2.1)};
		NotaFiscalBuilder builder = new NotaFiscalBuilder(Arrays.asList(acoes));

		NotaFiscal nf = builder.paraEmpresa("Alice").comCnpj("123123123")
				.com(new ItemDaNota("caneta", 2.5)).constroi();
		
		System.out.println(nf.getValorBruto());
	}
}
