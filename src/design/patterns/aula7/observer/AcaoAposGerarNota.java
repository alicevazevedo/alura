package design.patterns.aula7.observer;

import design.patterns.aula6.builder.NotaFiscal;

public interface AcaoAposGerarNota {
	void executa(NotaFiscal nf);
}
