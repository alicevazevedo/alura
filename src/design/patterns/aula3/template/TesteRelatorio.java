package design.patterns.aula3.template;

import java.util.ArrayList;
import java.util.List;

import design.patterns.aula2.chain.Conta;


public class TesteRelatorio {
	public static void main(String[] args) {
		Banco banco = new Banco("Banco Itau", "(21) 2121-2121", "Rua Imaginarios", "itau@itau.com.br");
		Conta conta = new Conta("Titular1", 200, "3530", "000000");
		Conta conta2 = new Conta("Titular2", 700, "3530", "000001");
		Conta conta3 = new Conta("Titular3", 2000, "3530", "000002");
		List<Conta> contas = new ArrayList<Conta>();
		contas.add(conta);
		contas.add(conta2);
		contas.add(conta3);
		
		RelatorioSimples simples = new RelatorioSimples();
		simples.imprime(banco, contas);
		
		System.out.println("-------------------------------");
		
		RelatorioComplexo complexo = new RelatorioComplexo();
		complexo.imprime(banco, contas);
	}
}
