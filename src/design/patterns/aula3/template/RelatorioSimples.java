package design.patterns.aula3.template;

import java.util.List;

import design.patterns.aula2.chain.Conta;


public class RelatorioSimples extends TemplateDeRelatorio {
	@Override
	protected void cabecalho(Banco banco) {
		System.out.println(banco.getNome());
	}

	@Override
	protected void corpo(List<Conta> contas) {
		for (Conta conta : contas) {
			System.out.println(conta.getTitular() + " " + conta.getSaldo());
		}
	}

	@Override
	protected void rodape(Banco banco) {
		System.out.println(banco.getTelefone());
	}

}
