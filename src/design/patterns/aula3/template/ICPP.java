package design.patterns.aula3.template;

import design.patterns.aula1.strategy.Imposto;
import design.patterns.aula1.strategy.Orcamento;

public class ICPP extends TemplateDeImpostoCondicional {

	public ICPP() {
	}
	
	public ICPP(Imposto imposto) {
		super(imposto);
	}
	
	@Override
	public double minimaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * 0.05;
	}

	@Override
	public double maximaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * 0.07;
	}

	@Override
	public boolean deveUsarMaximaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() > 500;
	}
}
