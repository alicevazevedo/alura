package design.patterns.aula3.template;

import java.util.Date;
import java.util.List;

import design.patterns.aula2.chain.Conta;


public class RelatorioComplexo extends TemplateDeRelatorio {

	@Override
	protected void cabecalho(Banco banco) {
		System.out.println(banco.getNome() + " " + banco.getEndereco() + " " + banco.getTelefone());
	}

	@Override
	protected void corpo(List<Conta> contas) {
		for (Conta conta : contas) {
			System.out.println(conta.getTitular() + " " + conta.getAgencia() + " " + conta.getNumeroConta() + " " + conta.getSaldo());
		}
	}

	@Override
	protected void rodape(Banco banco) {
		System.out.println(banco.getEmail() + " " + new Date());
	}

}
