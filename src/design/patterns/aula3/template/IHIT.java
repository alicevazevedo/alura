package design.patterns.aula3.template;

import java.util.ArrayList;

import design.patterns.aula1.strategy.Item;
import design.patterns.aula1.strategy.Orcamento;


public class IHIT extends TemplateDeImpostoCondicional {

	@Override
	protected double minimaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * (orcamento.getItens().size() * 0.01);
	}

	@Override
	protected double maximaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * 0.13 + 100;
	}

	@Override
	protected boolean deveUsarMaximaTaxacao(Orcamento orcamento) {
		if (orcamento.getItens().size() >= 2) {
			ArrayList<String> lista = new ArrayList<String>();
			for (Item item : orcamento.getItens()) {
				if (lista.contains(item.getNome())) {
					return true;
				}
				lista.add(item.getNome());
			}
		}
		return false;
	}
}
