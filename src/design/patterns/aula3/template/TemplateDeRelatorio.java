package design.patterns.aula3.template;

import java.util.List;

import design.patterns.aula2.chain.Conta;


public abstract class TemplateDeRelatorio {
	
	public void imprime(Banco banco, List<Conta> contas) {
		cabecalho(banco);
		System.out.println();
		corpo(contas);
		System.out.println();
		rodape(banco);
	}
	
	protected abstract void cabecalho(Banco banco);
	protected abstract void corpo(List<Conta> contas);
	protected abstract void rodape(Banco banco);
}
