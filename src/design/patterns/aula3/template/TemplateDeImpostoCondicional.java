package design.patterns.aula3.template;

import design.patterns.aula1.strategy.Imposto;
import design.patterns.aula1.strategy.Orcamento;

public abstract class TemplateDeImpostoCondicional extends Imposto {

	public TemplateDeImpostoCondicional() {
	}
	
	public TemplateDeImpostoCondicional(Imposto imposto) {
		super(imposto);
	}

	@Override
	public final double calcular(Orcamento orcamento) {
		if (deveUsarMaximaTaxacao(orcamento)) {
			return maximaTaxacao(orcamento) + calculoDoOutroImposto(orcamento);
		}
		return minimaTaxacao(orcamento) + calculoDoOutroImposto(orcamento);
	}

	protected abstract double minimaTaxacao(Orcamento orcamento);

	protected abstract double maximaTaxacao(Orcamento orcamento);

	protected abstract boolean deveUsarMaximaTaxacao(Orcamento orcamento);

}
