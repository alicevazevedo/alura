package design.patterns.aula3.template;

import design.patterns.aula1.strategy.Item;
import design.patterns.aula1.strategy.Orcamento;

public class TesteTemplate {

	public static void main(String[] args) {
		Orcamento orcamento = new Orcamento(500);
		ICPP icpp = new ICPP();
		IKCV ikcv = new IKCV();
		
		System.out.println(icpp.calcular(orcamento));
		System.out.println(ikcv.calcular(orcamento));
		
		Orcamento orcamento2 = new Orcamento(600);
		orcamento2.adicionaItem(new Item("lapis", 150));
		
		System.out.println(icpp.calcular(orcamento2));
		System.out.println(ikcv.calcular(orcamento2));
	}
}
