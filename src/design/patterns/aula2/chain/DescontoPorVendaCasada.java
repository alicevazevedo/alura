package design.patterns.aula2.chain;
import design.patterns.aula1.strategy.Item;
import design.patterns.aula1.strategy.Orcamento;


public class DescontoPorVendaCasada implements Desconto {

	private Desconto proximo;
	@Override
	public double desconta(Orcamento orcamento) {
		if (existe("LAPIS", orcamento) && existe("CANETA", orcamento)) {
			return orcamento.getValor() * 0.05;
		}
		return this.proximo.desconta(orcamento);
	}
	private boolean existe(String nomeDoItem, Orcamento orcamento) {
        for (Item item : orcamento.getItens()) {
            if(item.getNome().equals(nomeDoItem)) return true;
        }
        return false;
    }
	@Override
	public void setProximo(Desconto proximo) {
		this.proximo = proximo;
	}
}
