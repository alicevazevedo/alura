package design.patterns.aula2.chain;
import design.patterns.aula1.strategy.Orcamento;


public interface Desconto {
	public double desconta(Orcamento orcamento);
	public void setProximo(Desconto proximo);
}
