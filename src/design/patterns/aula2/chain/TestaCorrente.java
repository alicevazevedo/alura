package design.patterns.aula2.chain;
import design.patterns.aula1.strategy.Orcamento;

public class TestaCorrente {
	public static void main(String[] args) {
		Desconto d1 = new DescontoPorCincoItens();
		Desconto d2 = new DescontoPorMaisDeQuinhentosReais();
		Desconto d3 = new DescontoPorVendaCasada();
		Desconto d4 = new SemDesconto();

		d1.setProximo(d2);
		d2.setProximo(d3);
		d3.setProximo(d4);

		Orcamento orcamento = new Orcamento(500.0);

		double desconto = d1.desconta(orcamento);
		System.out.println(desconto);
	}
}
