package design.patterns.aula2.chain;
import design.patterns.aula1.strategy.Item;
import design.patterns.aula1.strategy.Orcamento;

public class TesteDeDescontos {
	public static void main(String[] args) {
		CalculadorDeDescontos descontos = new CalculadorDeDescontos();
		
		Orcamento orcamento = new Orcamento(500);
		orcamento.adicionaItem(new Item("CANETA", 250));
		orcamento.adicionaItem(new Item("LAPIS", 250));
		
		double descontoFinal = descontos.calcula(orcamento);
		System.out.println(descontoFinal);
	}
}
