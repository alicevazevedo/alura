package design.patterns.aula2.chain;

public class RespostaPorcento implements Resposta {

	private Resposta proxima;
	
	public RespostaPorcento() {
		this.proxima = null;
	}
	
	public RespostaPorcento(Resposta proxima) {
		this.proxima = proxima;
	}

	@Override
	public void imprime(Requisicao requisicao, Conta conta) {
		if (requisicao.getFormato() == Formato.PORCENTO) {
			System.out.println(conta.getTitular() + "%" + conta.getSaldo());
		} else if (proxima != null) {
			proxima.imprime(requisicao, conta);
		}
	}
}
