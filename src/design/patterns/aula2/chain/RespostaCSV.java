package design.patterns.aula2.chain;

public class RespostaCSV implements Resposta {
	private Resposta proxima;
	
	public RespostaCSV() {
		this.proxima = null;
	}

	public RespostaCSV(Resposta proxima) {
		this.proxima = proxima;
	}
	
	@Override
	public void imprime(Requisicao requisicao, Conta conta) {
		if (requisicao.getFormato() == Formato.CSV) {
			System.out.println(conta.getTitular() + ";" + conta.getSaldo());
		} else if (proxima != null) {
			proxima.imprime(requisicao, conta);
        }
	}
}
