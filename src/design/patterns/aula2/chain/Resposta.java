package design.patterns.aula2.chain;

public interface Resposta {
	public void imprime(Requisicao requisicao, Conta conta);
}
