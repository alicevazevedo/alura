package design.patterns.aula2.chain;

import java.util.Date;

public class Conta {
	private String titular;
	private double saldo;
	private String agencia;
	private String numeroConta;
	private Date dataAbertura;

	public Conta(String titular, double saldo, String agencia,
			String numeroConta) {
		this.titular = titular;
		this.saldo = saldo;
		this.agencia = agencia;
		this.numeroConta = numeroConta;
	}

	public String getAgencia() {
		return agencia;
	}

	public String getNumeroConta() {
		return numeroConta;
	}

	public String getTitular() {
		return titular;
	}

	public double getSaldo() {
		return saldo;
	}
	
	public Date getDataAbertura() {
		return dataAbertura;
	}

}
