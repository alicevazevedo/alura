package design.patterns.aula2.chain;

public class RespostaXML implements Resposta {
	private Resposta proxima;

	public RespostaXML() {
		this.proxima = null;
	}
	
	public RespostaXML(Resposta proxima) {
		this.proxima = proxima;
	}
	
	@Override
	public void imprime(Requisicao requisicao, Conta conta) {
		if (requisicao.getFormato() == Formato.XML) {
			System.out.println("<conta><titular>" + conta.getTitular() + "</titular><saldo>" + conta.getSaldo() + "</saldo></conta>");
		} else if (proxima != null) {
			this.proxima.imprime(requisicao, conta);
		}
	}
}
