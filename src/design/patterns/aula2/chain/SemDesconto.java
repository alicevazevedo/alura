package design.patterns.aula2.chain;
import design.patterns.aula1.strategy.Orcamento;


public class SemDesconto implements Desconto {

	@Override
	public double desconta(Orcamento orcamento) {
		return 0;
	}

	@Override
	public void setProximo(Desconto proximo) {
	}
}
