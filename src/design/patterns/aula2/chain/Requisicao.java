package design.patterns.aula2.chain;

public class Requisicao {
	private Formato formato;
	
    public Requisicao(Formato formato) {
      this.formato = formato;
    }
    
	public Formato getFormato() {
		return formato;
	}
}
