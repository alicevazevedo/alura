package design.patterns.aula6.builder;

import java.util.ArrayList;
import java.util.List;

import design.patterns.aula7.observer.AcaoAposGerarNota;


public class TesteDaNotaFiscal {
	public static void main(String[] args) {
		List<AcaoAposGerarNota> acoes = new ArrayList<AcaoAposGerarNota>();
		
		NotaFiscalBuilder builder = new NotaFiscalBuilder(acoes)
		.paraEmpresa("empresa")
		.comCnpj("1233213123")
		.com(new ItemDaNota("item 1", 200))
		.com(new ItemDaNota("item 2", 300))
		.com(new ItemDaNota("item 3", 400))
		
		.comObservacoes("bla bla");
		
		NotaFiscal nota = builder.constroi();
		System.out.println(nota.getValorBruto());
	}
}
