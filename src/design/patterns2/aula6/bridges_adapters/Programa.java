package design.patterns2.aula6.bridges_adapters;

import java.util.Calendar;

public class Programa {

	/*
	 * Bridge visa chegar em outro sistema
	 * Strategy sao algoritmos diferentes
	 * Adapter usado quando tem-se um conjunto de classes legadas.
	 */
	
	/*
	 A diferen�a � sem�ntica. 
	 A ideia da Bridge � justamente ser uma ponte em dois mundos/sistemas. 
	 A ideia do Adapter � esconder alguma "sujeira", ou adaptar algo que � diferente e n�o bate com o sistema atual.
	 � bem comum inclusive que a interface do Adapter j� tenha sido pr�-definida e j� at� exista no sistema. 
	 Nessa situa��o, o programador vai l� para implementar um "adaptador" para o sistema antigo, de forma a caber no modelo j� existente.
	 */
	public static void main(String[] args) {
		Mapa mapa = new GoogleMaps();
		mapa.devolverMapa("prudente de morais");
		
		///
		
		RelogioDoSistema relogio  = new RelogioDoSistema();
		Calendar hoje = relogio.hoje();
	}
}
