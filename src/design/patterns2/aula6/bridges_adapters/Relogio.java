package design.patterns2.aula6.bridges_adapters;

import java.util.Calendar;

public interface Relogio {
	public Calendar hoje();
}