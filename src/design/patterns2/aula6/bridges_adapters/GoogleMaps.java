package design.patterns2.aula6.bridges_adapters;

import java.io.InputStream;
import java.net.URL;

public class GoogleMaps implements Mapa {

	@Override
	public String devolverMapa(String rua) {
		String googleMaps = "http://maps.google.com.br/maps?q=" + rua;
		URL url;
		try {
			url = new URL(googleMaps);
			InputStream openStream = url.openStream();
			// ...
			return "mapa";
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
