package design.patterns2.aula3.memento;

public enum TipoContrato {
	NOVO,
	EM_ANDAMENTO,
	ACERTADO,
	CONCLUIDO
}
