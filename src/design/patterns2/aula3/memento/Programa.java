package design.patterns2.aula3.memento;

import java.util.Calendar;

public class Programa {

	public static void main(String[] args) {
		Historico historico = new Historico();
		
		Contrato c = new Contrato(Calendar.getInstance(), "Alice", TipoContrato.NOVO);
		historico.adiciona(c.salvaEstado());
		
		c.avanca();
		historico.adiciona(c.salvaEstado());
		
		c.avanca();
		historico.adiciona(c.salvaEstado());
		
		Estado estadoAnterior = historico.pega(0);
		System.out.println(estadoAnterior.getEstado().getTipo());
	}
}
