package design.patterns2.aula1.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class FabricaDeConexao {
	
	public Connection criarConexao() {
		try {
			
			String banco = new Properties().getProperty("tipo.banco");
			
			Connection conexao;
			if ("mysql".equalsIgnoreCase(banco)) {
				conexao = 
						DriverManager.getConnection("jdbc:mysql://localhost:3306/bd", "admin", "admin");
			} else if ("postgres".equalsIgnoreCase(banco)) {
				conexao = 
						DriverManager.getConnection("jdbc:postgres://localhost:3306/bd", "admin", "admin");
			} else {
				throw new RuntimeException("Tipo de banco nao reconhecido: " + banco);
			}

			return conexao;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
	}
}
