package design.patterns2.aula2.flyweight;

import java.util.Arrays;
import java.util.List;

public class Programa {
	public static void main(String[] args) {
		NotasMusicais notas = new NotasMusicais();
		
		List<Nota> musica = Arrays.asList(
				notas.pega("Do"),
				notas.pega("DoSustenido"),
				notas.pega("ReSustenido"),
				notas.pega("Mi"),
				notas.pega("Fa"),
				notas.pega("Fa"),
				notas.pega("Fa"));
		
		Piano piano = new Piano();
		piano.toca(musica);
	}
}
