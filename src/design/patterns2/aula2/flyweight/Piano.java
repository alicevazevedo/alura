package design.patterns2.aula2.flyweight;

import java.util.List;

import org.jfugue.Player;

public class Piano {

	public void toca(List<Nota> musica) {
		Player player = new Player();
		
		StringBuilder partitura = new StringBuilder();
		
		for (Nota nota : musica) {
			partitura.append(nota.simbolo() + " ");
		}
		
		System.out.println(partitura);
		player.play(partitura.toString());
	}
}
