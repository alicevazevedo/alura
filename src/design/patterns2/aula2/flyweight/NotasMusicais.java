package design.patterns2.aula2.flyweight;

import java.util.HashMap;
import java.util.Map;

public class NotasMusicais {
	
	private static Map<String, Nota> notas = new HashMap<String, Nota>();
	
	static {
		/*notas.put("do", new Do());
		notas.put("re", new Re());
		notas.put("mi", new Mi());
		notas.put("fa", new Fa());
		notas.put("sol", new Sol());
		notas.put("la", new La());
		notas.put("si", new Si());
		*/
	}
	
	public Nota pega(String nome) {
		if (!notas.containsKey(nome)) {
			try {
				String packageName = this.getClass().getPackage().getName() + ".";
				Class clazz = Class.forName(packageName + nome);
				if (Nota.class.isAssignableFrom(clazz)) {
					notas.put(nome, (Nota) clazz.newInstance());
				}
			} catch (Exception e) {
				throw new RuntimeException("Nao ha nota com nome " + nome);
			}
		}
		return notas.get(nome);
	}
}
