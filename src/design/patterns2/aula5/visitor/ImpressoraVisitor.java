package design.patterns2.aula5.visitor;

import design.patterns2.aula4.dsls_interpreter.Divisao;
import design.patterns2.aula4.dsls_interpreter.Multiplicacao;
import design.patterns2.aula4.dsls_interpreter.Numero;
import design.patterns2.aula4.dsls_interpreter.RaizQuadrada;
import design.patterns2.aula4.dsls_interpreter.Soma;
import design.patterns2.aula4.dsls_interpreter.Subtracao;

public class ImpressoraVisitor implements Visitor {

	@Override
	public void visitaSoma(Soma soma) {
		System.out.print("(");
		
		// imprime esquerda
		soma.getEsquerda().aceita(this);
		
		System.out.print("+");
		
		// imprime direita
		soma.getDireita().aceita(this);
		
		System.out.print(")");
	}
	
	@Override
	public void visitaSubtracao(Subtracao subtracao) {
		System.out.print("(");
		
		// imprime esquerda
		subtracao.getEsquerda().aceita(this);
		
		System.out.print("-");
		
		// imprime direita
		subtracao.getDireita().aceita(this);
		
		System.out.print(")");
	}
	

	@Override
	public void visitaNumero(Numero numero) {
		System.out.print(numero.getNumero());
	}

	@Override
	public void visitaMultiplicacao(Multiplicacao multiplicacao) {
		System.out.print("(");
		
		// imprime esquerda
		multiplicacao.getEsquerda().aceita(this);
		
		System.out.print("*");
		
		// imprime direita
		multiplicacao.getDireita().aceita(this);
		
		System.out.print(")");
	}

	@Override
	public void visitaDivisao(Divisao divisao) {
		System.out.print("(");
		
		// imprime esquerda
		divisao.getEsquerda().aceita(this);
		
		System.out.print("/");
		
		// imprime direita
		divisao.getDireita().aceita(this);
		
		System.out.print(")");
	}

	@Override
	public void visitaRaizQuadrada(RaizQuadrada raizQuadrada) {
		System.out.print("(");
		
		System.out.print("\u221A");
		raizQuadrada.getExpressao().aceita(this);
		
		System.out.print(")");
	}
}
