package design.patterns2.aula5.visitor;

import design.patterns2.aula4.dsls_interpreter.Divisao;
import design.patterns2.aula4.dsls_interpreter.Multiplicacao;
import design.patterns2.aula4.dsls_interpreter.Numero;
import design.patterns2.aula4.dsls_interpreter.RaizQuadrada;
import design.patterns2.aula4.dsls_interpreter.Soma;
import design.patterns2.aula4.dsls_interpreter.Subtracao;

public interface Visitor {

	public abstract void visitaSoma(Soma soma);

	public abstract void visitaSubtracao(Subtracao subtracao);

	public abstract void visitaNumero(Numero numero);

	public abstract void visitaMultiplicacao(Multiplicacao multiplicacao);
	
	public abstract void visitaDivisao(Divisao divisao);
	
	public abstract void visitaRaizQuadrada(RaizQuadrada raizQuadrada);
}