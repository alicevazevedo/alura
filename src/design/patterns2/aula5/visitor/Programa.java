package design.patterns2.aula5.visitor;

import design.patterns2.aula4.dsls_interpreter.Expressao;
import design.patterns2.aula4.dsls_interpreter.Numero;
import design.patterns2.aula4.dsls_interpreter.RaizQuadrada;
import design.patterns2.aula4.dsls_interpreter.Soma;
import design.patterns2.aula4.dsls_interpreter.Subtracao;

public class Programa {

	public static void main(String[] args) {
		Expressao esquerda = new Subtracao(new Numero(10), new Numero(5)); 
		Expressao direita = new Soma(new Numero(2), new Numero(10)); 
		Expressao soma = new Soma(esquerda, direita);
		Expressao raizQ = new RaizQuadrada(soma);
		
		
		Visitor impressora = new ImpressoraVisitor();
		raizQ.aceita(impressora);
		System.out.println();
		
		Visitor preFix = new ImpressoraPreFixVisitor();
		raizQ.aceita(preFix);
		
		int resultado = raizQ.avalia();
		System.out.println(resultado);
	}
}
