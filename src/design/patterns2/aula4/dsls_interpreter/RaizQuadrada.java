package design.patterns2.aula4.dsls_interpreter;

import design.patterns2.aula5.visitor.Visitor;

public class RaizQuadrada implements Expressao {

	private Expressao expressao;

	public RaizQuadrada(Expressao expressao) {
		this.expressao = expressao;
	}
	
	@Override
	public int avalia() {
		int valor = expressao.avalia();
		return (int) Math.sqrt(valor);
	}
	
	public Expressao getExpressao() {
		return expressao;
	}

	@Override
	public void aceita(Visitor visitor) {
		visitor.visitaRaizQuadrada(this);
	}

}
