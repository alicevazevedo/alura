package design.patterns2.aula4.dsls_interpreter;

import design.patterns2.aula5.visitor.Visitor;

// Padrao: interpreter
public interface Expressao {
	public int avalia();
	
	// padrao eh chamar esse metodo de aceita
	public void aceita(Visitor visitor);
}
