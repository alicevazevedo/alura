package design.patterns2.aula7.command;

public class ConcluiPedido implements Comando {
	
	private Pedido pedido;

	public ConcluiPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	@Override
	public void executa() {
		System.out.println("Concluindo pedido " + pedido.getCliente());
		pedido.finaliza();
	}

}
