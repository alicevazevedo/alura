package design.patterns2.aula7.command;

public enum Status {
	NOVO, PROCESSANDO, PAGO, ITEM_SEPARADO, ENTREGUE
}
