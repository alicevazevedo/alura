package design.patterns2.aula7.command;

public interface Comando {
	void executa();
}
