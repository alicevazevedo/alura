package design.patterns2.aula7.command;

public class Programa {

	public static void main(String[] args) {
		Pedido p1 = new Pedido("Alice", 150);
		Pedido p2 = new Pedido("Saulo", 250);
		
		FilaDeTrabalho filaDeTrabalho = new FilaDeTrabalho();
		filaDeTrabalho.adiciona(new PagaPedido(p1));
		filaDeTrabalho.adiciona(new PagaPedido(p2));
		filaDeTrabalho.adiciona(new ConcluiPedido(p1));
		
		filaDeTrabalho.processa();
	}
}
